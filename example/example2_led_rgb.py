# Przykład demonstruje użycie diody LED RGB

# import bibliotek
from sumolib import *
import time

print('Przyklad 2')

# przycisk START1
start1 = Start1()

# dioda LED_RGB1
ledRgb1 = LedRgb1()

# ustawienie jesności
ledRgb1.brightness = 0.3

# kolory które będą zmieniane
colors = [Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE] 

print('Kolory z listy')

# pętla działająca do naciśnięcia przycisku START1
while not start1.value:
    # pętla po kolorach
    for color in colors:
        # ustawienie koloru
        ledRgb1.value = color
        # opóźnienie w sekundach
        time.sleep(0.5)
        
# zmiana jesności
ledRgb1.brightness = 0.9       
        
print('Gradient')

# pętla nieskończona
while True:
    # przejście z niebieskiego do czerwonego
    # pętla dla każdego i z zakresu
    for i in range(200):
        color = (i, 0, 200-i)
        # ustawienie koloru i opóźnienie
        ledRgb1.value = color
        time.sleep(0.02)
        
    # przejście z czerwonego do zielonego
    # pętla dla każdego i z zakresu
    for i in range(200):
        color = (200-i, i, 0)
        # ustawienie koloru i opóźnienie
        ledRgb1.value = color
        time.sleep(0.02)
        
    # przejście z zielonego do niebieskiego
    # pętla dla każdego i z zakresu
    for i in range(200):
        color = (0, 200-i, i)
        # ustawienie koloru i opóźnienie
        ledRgb1.value = color
        time.sleep(0.02)
