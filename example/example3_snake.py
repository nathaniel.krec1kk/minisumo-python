# Przykład demonstruje użycie wielu diod LED RGB

# Do złacza EXT2 należy podłączyc pasek diod LED WS2812B

# import bibliotek
from sumolib import *
import time

print('Przyklad 3')

# dioda LED_RGB1
ledRgb1 = LedRgb1()

# liczba diod
LED_NUMBER = 50

# pętla nieskończona
while True:
    # pętla po liczbie diod
    for i in range(LED_NUMBER + 1):
        # tworzenie listy z kolorami niebieskimi
        blues = [Color.BLUE] * i
        # tworzenie listy z kolorami czerwonymi
        reds = [Color.RED] * (LED_NUMBER + 1 - i)
        # lista kolorów - łączy powyższe
        colors = blues + reds
        # ustawienie kolorów i opóźnienie
        ledRgb1.values = colors
        time.sleep(0.2)