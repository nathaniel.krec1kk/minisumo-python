# Przykład demonstruje działanie silników

# UWAGA! wartość power to wartość w zakresie 0-1
# Decyduje ona o napięciu podawanym na silniki

# import bibliotek
from sumolib import *
import time

print('Przyklad 6')

# silniki
motor1, motor2 = motors_init()
# przycisk START1
start1 = Start1()
# przycisk BOOT1
boot1 = Boot1()
# diody
led1 = Led1()
led2 = Led2()
ledRgb1 = LedRgb1()

ledRgb1.brightness = 0.3
ledRgb1.value = Color.RED

# ustawianie mocy silników
motor1.power = 0.4
motor2.power = 0.5

# oczekiwanie na naciśnięcie przycisku
start1.waitFor()

print('Start')
ledRgb1.value = Color.GREEN

# pętla nieskończona
while True:
    # jeśli jest naciśnięty przycisk START1
    if start1.value:
        # MOTOR1 do przodu
        motor1.forward()
        # MOTOR2 do tyłu
        motor2.backward()
        # sygnalizacja
        ledRgb1.value = Color.GREEN
    # jeśli jest naciśnięty przycisk BOOT1
    elif boot1.value:
        # MOTOR1 do tyłu
        motor1.backward()
        # MOTOR2 do przodu
        motor2.forward()
        # sygnalizacja
        ledRgb1.value = Color.WHITE
    # w przeciwnym wypadku
    else:
        # MOTOR1 stop
        motor1.stop()
        # MOTOR2 hamowanie silnikiem
        motor2.motorBreak()
        # sygnalizacja
        ledRgb1.value = Color.CYAN
    time.sleep(0.1)