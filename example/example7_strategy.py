# Przykładowy program walki robota

# Program wykorzystuje 4 czujniki podłoża, 3 czujniki odległości Shar, oraz 2 silniki
# Konfiguracja
# GRD1 - czujni podłoża lewy przedni
# GRD2 - czujni podłoża prawy przedni
# GRD1 - czujni podłoża lewy tylny
# GRD2 - czujni podłoża prawy tylny
# DIST1 - czujnik odległości lewy przedni
# DIST2 - czujnik odległości prawy przedni
# DIST3 - czujnik odległości centralny tylny
# MOTOR1 - silnik lewy
# MOTOR2 - silnik prawy
# START1 - przycisk startujący
# LED1 - pokazuje stan czujnika GRD1
# LED2 - pokazuje stan czujnika GRD2

# import bibliotek
from sumolib import *
import time


# przycisk START1
start1 = Start1()
# diody
led1 = Led1()
led2 = Led2()
ledRgb1 = LedRgb1()
# czujniki podłoża
grd1, grd2, grd3, grd4 = grds_init()
# czujniki odległości
dist1, dist2, dist3, dist4 = dists_init()
# silniki
motor1, motor2 = motors_init()

# stałe
# próg dla czujników podłoża
GRD_TRESHOLD = 0.7
# próg dla czujników odległości
DIST_TRESHOLD = 0.6
# moc silników przy szukaniu przeciwnika
SEARCH_POWER = 0.4
# moc silników przy ataku
ATACK_POWER = 0.6

# wykonywane akcje
# do przodu
def forward():
    motor1.power = SEARCH_POWER
    motor1.forward()
    motor2.power = SEARCH_POWER
    motor2.forward()
    ledRgb1.value = Color.WHITE

# do przodu z większą mocą
def forward_atack():
    motor1.power = ATACK_POWER
    motor1.forward()
    motor2.power = ATACK_POWER
    motor2.forward()
    ledRgb1.value = Color.WHITE

# do przodu na lewo
def forward_left():
    motor1.power = SEARCH_POWER / 3
    motor1.forward()
    motor2.power = SEARCH_POWER
    motor2.forward()
    ledRgb1.value = Color.WHITE
    
# do przodu na prawo
def forward_right():
    motor1.power = SEARCH_POWER 
    motor1.forward()
    motor2.power = SEARCH_POWER / 3
    motor2.forward()
    ledRgb1.value = Color.WHITE
    
# do tyłu
def backward():
    motor1.power = SEARCH_POWER
    motor1.backward()
    motor2.power = SEARCH_POWER
    motor2.backward()
    ledRgb1.value = Color.RED
    
# do tyłu z większą mocą
def backward_atack():
    motor1.power = ATACK_POWER
    motor1.backward()
    motor2.power = ATACK_POWER
    motor2.backward()
    ledRgb1.value = Color.RED

# do tyłu na lewo
def backward_left():
    motor1.power = SEARCH_POWER / 3
    motor1.backward()
    motor2.power = SEARCH_POWER
    motor2.backward()
    ledRgb1.value = Color.RED
    
# do tyłu na prawo
def backward_right():
    motor1.power = SEARCH_POWER 
    motor1.backward()
    motor2.power = SEARCH_POWER / 3
    motor2.backward()
    ledRgb1.value = Color.RED
    
# stop
def stop(): 
    motor1.stop()
    motor2.stop()
    ledRgb1.value = Color.GREEN
    
# obrót w lewo
def rotate_left():
    motor1.power = SEARCH_POWER
    motor1.backward()
    motor2.power = SEARCH_POWER
    motor2.forward()
    ledRgb1.value = Color.YELLOW

# obrót w prawo
def rotate_right():
    motor1.power = SEARCH_POWER
    motor1.forward()
    motor2.power = SEARCH_POWER
    motor2.backward()
    ledRgb1.value = Color.YELLOW

# odliczanie
def countdown():
    print('5...')
    for i in range(10):
        ledRgb1.value = (250 - i*5, i*5, 0)
        time.sleep(0.1)
    print('4...')
    for i in range(10):
        ledRgb1.value = (200 - i*5, 50 + i*5, 0)
        time.sleep(0.1)
    print('3...')
    for i in range(10):
        ledRgb1.value = (150 - i*5, 100 + i*5, 0)
        time.sleep(0.1)
    print('2...')
    for i in range(10):
        ledRgb1.value = (100 - i*5, 150 + i*5, 0)
        time.sleep(0.1)
    print('1...')
    for i in range(10):
        ledRgb1.value = (50 - i*5, 200 + i*5, 0)
        time.sleep(0.1)
    print('START!!!')

# podejmowanie decyzji przez robota
def decide():
    # dla ułatwienia progowanie czujników
    d1 = dist1.value > DIST_TRESHOLD
    d2 = dist2.value > DIST_TRESHOLD
    d3 = dist3.value > DIST_TRESHOLD
    g1 = grd1.value < GRD_TRESHOLD
    g2 = grd2.value < GRD_TRESHOLD
    g3 = grd3.value < GRD_TRESHOLD
    g4 = grd4.value < GRD_TRESHOLD
    # wyświetlanie na diodach
    led1.value = g1
    led2.value = g2
    # właściwy algorytm
    
    # jeżeli którykolwiek czujnik wykrywa linię
    if g1 or g2 or g3 or g4:
        # rozpatrywanie przypadków
        if g1 and g2:
            # linia z przodu
            backward()
        elif g3 and g4:
            # linia z tyłu
            forward()
        elif g1:
            backward_right()
        elif g2:
            backward_left()
        elif g3:
            forward_right()
        elif g4:
            forward_left()
    # brak linii
    
    # jeżeli którykolwiek czujnik wykrywa przeciwnika
    elif d1 or d2 or d3:
        # przeciwnik na wprost
        if d1 and d2:
            forward_atack()
        # przeciwnik z przodu po lewej
        elif d1:
            forward_left()
        # przeciwnik z przodu po prawej
        elif d2:
            forward_right()
        # przeciwnik z tyłu
        elif d3:
            rotate_left()
            
    # brak przeciwnika
    else:
        # przeszukiwanie jazdą po łuku
        forward_left()

# program właściwy
print('Witaj, oczekiwanie na start')
ledRgb1.value = Color.CYAN
# oczekiwanie na start
start1.waitFor()
# odliczanie
countdown()
# walka
while(True):
    decide()
    time.sleep(0.02)
